public class CircleClass {
    private String name;
    private double radian;

    public CircleClass(String name, double radian) {
        this.name = name;
        this.radian = radian;
    }

    public void print() {
        System.out.println("Name: " + name + " " + "circleArea: " + circleArea());
        System.out.println("Name: " + name + " " + "circleParimeter: " + circlePerimeter());
    }

    public double circleArea() {
        double ca = Math.PI * (radian * radian);
        return ca;
    }

    public double circlePerimeter() {
        double cp = 2 * (22 / 7) * radian;
        return cp;
    }
}
