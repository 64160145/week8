public class TriangleClass {
    private String name;
    private double a;
    private double b;
    private double c;

    public TriangleClass(String name, double a, double b, double c) {
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public void print() {
        System.out.println("Name: " + name + " " + "triangleArea: " + triangleArea());
        System.out.println("Name: " + name + " " +"trianglePerimeter: "+ trianglePerimeter());
    }

    public double triangleArea() {
        double s = a + b + c / 2;
        double ta = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        return ta;
    }

    public double trianglePerimeter() {
        double tp = a + b + c;
        return tp;
    }
}
