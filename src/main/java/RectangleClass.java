public class RectangleClass {
    private String name;
    private double width;
    private double height;

    public RectangleClass(String name, double width, double height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public void print() {
        System.out.println("Name: "+name + " " +"rectangleArea: "+rectangleArea());
        System.out.println("Name: "+name + " " +"rectanglePerimeter: "+ rectanglePerimeter());
    }
    
    public double rectanglePerimeter(){
        double rp = 2*(width*height);
        return rp;
    }

    public double rectangleArea(){
        double ra = width*height;
        return ra;
    }
}
