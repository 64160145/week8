public class TestShape {
    public static void main(String[] args) {
        RectangleClass rect1 = new RectangleClass("rect1", 10, 5);
        rect1.print();
        RectangleClass rect2 = new RectangleClass("rect2", 5, 3);
        rect2.print();

        CircleClass circle1 = new CircleClass("circle1", 1);
        circle1.print();
        CircleClass circle2 = new CircleClass("circle2", 2);
        circle2.print();

        TriangleClass tri1 = new TriangleClass("tri1", 5, 5, 6);
        tri1.print();
    }
}
